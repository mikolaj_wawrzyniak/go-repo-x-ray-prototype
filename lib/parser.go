package lib

import (
	"context"

	sitter "github.com/smacker/go-tree-sitter"
	"github.com/smacker/go-tree-sitter/javascript"
)

type TreeInfo struct {
	Classes   []string
	Functions []string
}

func ParseFileTree(file File) TreeInfo {
	codeAsBytes := []byte(file.Content)

	lang := javascript.GetLanguage()
	node, _ := sitter.ParseCtx(context.Background(), codeAsBytes, lang)
	qc := sitter.NewQueryCursor()

	query := `
	(
		(comment)* @doc
		.
		[
		  (class
			name: (_) @name)
		  (class_declaration
			name: (_) @name)
		] @definition.class
		(#strip! @doc "^[\\s\\*/]+|^[\\s\\*/]$")
	  )

	 (	
		(comment)* @doc
		.
		(method_definition
		  name: (property_identifier) @name) @definition.method
		(#not-eq? @name "constructor")
		(#strip! @doc "^[\\s\\*/]+|^[\\s\\*/]$")
		(#select-adjacent! @doc @definition.method)
	  )
	
	  (
		(comment)* @doc
		.
		[
		  (function
			name: (identifier) @name)
		  (function_declaration
			name: (identifier) @name)
		  (generator_function
			name: (identifier) @name)
		  (generator_function_declaration
			name: (identifier) @name)
		] @definition.function
		(#strip! @doc "^[\\s\\*/]+|^[\\s\\*/]$")
		(#select-adjacent! @doc @definition.function)
	  )
	
	  (
		(comment)* @doc
		.
		(lexical_declaration
		  (variable_declarator
			name: (identifier) @name
			value: [(arrow_function) (function)]) @definition.function)
		(#strip! @doc "^[\\s\\*/]+|^[\\s\\*/]$")
		(#select-adjacent! @doc @definition.function)
	  )
	
	  (
		(comment)* @doc
		.
		(variable_declaration
		  (variable_declarator
			name: (identifier) @name
			value: [(arrow_function) (function)]) @definition.function)
		(#strip! @doc "^[\\s\\*/]+|^[\\s\\*/]$")
		(#select-adjacent! @doc @definition.function)
	  )
	
	  (assignment_expression
		left: [
		  (identifier) @name
		  (member_expression
			property: (property_identifier) @name)
		]
		right: [(arrow_function) (function)]
	  ) @definition.function	
  	`

	q, _ := sitter.NewQuery([]byte(query), lang)

	treeInfo := TreeInfo{
		Functions: make([]string, 0),
		Classes:   make([]string, 0),
	}

	qc.Exec(q, node)

	for {
		m, ok := qc.NextMatch()
		if !ok {
			break
		}
		// Apply predicates filtering
		m = qc.FilterPredicates(m, codeAsBytes)

		// fmt.Println("--------------------------")
		// fmt.Printf("q.CaptureNameForId(capture.Id): %v\n", q.CaptureNameForId(m.ID))

		// if q.CaptureNameForId(m.ID) == "definition.class" {
		// 	fmt.Printf("m.Captures: %v\n", m.Captures)
		// }

		for _, capture := range m.Captures {
			// fmt.Printf("capture.Node.Type(): %v\n", capture.Node.Type())
			node_type := capture.Node.Type()

			if node_type == "function_declaration" || node_type == "method_definition" {
				name := capture.Node.ChildByFieldName("name").Content(codeAsBytes)
				// fmt.Printf("Function name: %s\n", name)
				treeInfo.Functions = append(treeInfo.Functions, name)
			}
			if capture.Node.Type() == "class_declaration" {
				name := capture.Node.ChildByFieldName("name").Content(codeAsBytes)
				// fmt.Printf("class name: %s\n", name)
				treeInfo.Classes = append(treeInfo.Classes, name)
			}
		}
	}

	return treeInfo
}
