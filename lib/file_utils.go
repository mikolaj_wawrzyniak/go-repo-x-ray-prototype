package lib

import (
	"os"
	"path/filepath"
)

type File struct {
	FilePath string
	Content  string
}

// function that takes two strings and return file

// a function that reads file content from appointed URI
// readFileContents reads the contents of the file at the given path
func readFileContents(filePath string) File {
	// Open the file
	file, err := os.Open(filePath)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// Read the file contents into a byte slice
	fileContents := make([]byte, 1024)
	_, err = file.Read(fileContents)
	if err != nil {
		panic(err)
	}

	fileData := File{
		FilePath: filePath,
		Content:  string(fileContents),
	}

	// Print the file contents
	// fmt.Println(fileData.FilePath)

	return fileData
}

// generate function that recursively reads files from appointed URI
func ScanFiles(root string) []File {
	files := []File{}

	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			files = append(files, readFileContents(path))
		}
		return nil
	})
	if err != nil {
		panic(err)
	}
	return files
}
