package lib

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/tmc/langchaingo/llms"
	anthropic "github.com/tmc/langchaingo/llms/anthropic"

	_ "github.com/mattn/go-sqlite3"
)

type FileSummary struct {
	FileSummary   string `json:"file_summary"`
	Language      string `json:"language"`
	ContainsTests bool   `json:"contains_tests"`

	LanguageSpecific struct {
		JSVersion string `json:"js_version"`
		TypeOfJS  string `json:"type_of_js"`
	} `json:"language_specific"`

	ProjectSpecific struct {
		UsesJQuery    bool `json:"uses_jquery"`
		ES7Compatible bool `json:"es7_compatible"`
	} `json:"project_specific"`

	Functions []struct {
		FunctionName        string `json:"function_name"`
		FunctionDescription string `json:"function_description"`
		FunctionParameters  []struct {
			Name          string `json:"name"`
			Description   string `json:"description"`
			ParameterType string `json:"parameter_type"`
		} `json:"function_parameters"`
	} `json:"functions"`

	Classes []struct {
		ClassName        string `json:"class_name"`
		ClassDescription string `json:"class_description"`
	} `json:"classes"`
}

func AddLLMInfo(treeInfo TreeInfo, file File) (FileSummary, error) {
	// Create Claude LLM

	claude, _ := anthropic.New()
	ctx := context.Background()

	aiMessage := "{\"file_summary\":"
	humanMessage := getJSPrompt(file.FilePath, file.Content, treeInfo.Functions, treeInfo.Classes)
	// humanMessage := "Who was the first man to walk on the moon?"
	completion, _ := claude.Call(ctx, "Human: "+humanMessage+"\n\nAssistant: "+aiMessage,
		llms.WithTemperature(0.1),
		llms.WithModel("claude-instant-1.2"),
		llms.WithMaxTokens(16000),
	)

	completionWithPrexif := aiMessage + completion
	// var llmInfo map[string]interface{}
	var llmInfo FileSummary
	err := json.Unmarshal([]byte(completionWithPrexif), &llmInfo)
	if err != nil {
		fmt.Println("Failed to parse JSON response")
		return llmInfo, err
	}

	fmt.Printf("llmInfo[\"functions\"]: %v\n", llmInfo.Functions)

	return llmInfo, nil
}

func getJSPrompt(filePath string, code string, functionList []string, classList []string) string {

	var functionSection string
	if len(functionList) > 0 {
		functionSection = `Please analyse the following functions and methods in the file:
    ` + strings.Join(functionList, "\n")
	}

	var classSection string
	if len(classList) > 0 {
		classSection = `Add descriptions for the following classes:
    ` + strings.Join(classList, "\n")
	}

	prompt := fmt.Sprintf(`
  As a JavaScript engineer, your task is to provide a summary of the code file located at '%s' and each individual function within the file. 
  
  <code>
  %s
  </code>

  %s

  %s

  Format the summary as valid JSON in the following way:
  {
   "file_summary": "(Description of the code in the file)",
   "language": "(language)",
   "contains_tests": "(boolean that shows if this file contains tests or specs)",        
   "language_specific": {
     "js_version":"(Javascript version, ES6 for example)",
     "type_of_js":"(Frontend, Node or Both)"
  },
  "project_specific": {
     "uses_jquery":"(boolean if this file uses jquery)",
     "es7_compatible":"(Is the file ES7 compatible)"
  },
  "functions": [
    {
        "function_name":"(Name of the function)",
        "function_description":"(Description of the function in maximum 5 sentences)",
        "function_parameters": [
            {
                "name":"(Name of the parameter)",
                "description":"(description of the parameter)",
                "parameter_type":"(javascript type of the parameter - string, object, number, boolean, bigint, symbol or undefined)"
            }
        ]
    }
  ],
  "classes": [
    {
        "class_name":"(Name of the class)",
        "class_description":"(Description of the class in maximum 5 sentences)",
    }
  ]
  }

  Please ensure that your summary accurately describes the content and purpose of the file and each individual function within it. The summary should be formatted as valid JSON and include details such as the language, version of JavaScript, and type of file. Additionally, please provide information on any library or file imports, public exports, and function parameters.
  `, filePath, code, functionSection, classSection)

	return prompt
}
