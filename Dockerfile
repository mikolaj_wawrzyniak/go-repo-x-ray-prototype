FROM golang:1.20.5

WORKDIR /usr/src/app

COPY . .

RUN go mod download
RUN go build cmd/scan.go

ENV PATH "$PATH:/usr/src/app/"

CMD ["scan", "."]