package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"database/sql"

	"gitlab.com/mikolaj_wawrzyniak/go-repo-x-ray-prototype/lib"

	_ "github.com/mattn/go-sqlite3"
)

type CodeInfo struct {
	Name          string
	FilePath      string
	DirectoryPath string
	Classes       []ClassInfo
	Functions     []FunctionInfo
	Characters    int
	Lines         int
	FileSummary   string
}

type ClassInfo struct {
	ClassName        string
	ClassDescription string
}

type FunctionInfo struct {
	FunctionName        string
	FunctionDescription string
}

func setupDb() *sql.DB {

	dbPath := "./database.db"
	// Open file, this will create it if not exists
	file, err := os.OpenFile(dbPath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Fatal(err)
	}

	file.Close()
	db, _ := sql.Open("sqlite3", dbPath)

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS files
		             (id INTEGER PRIMARY KEY, name TEXT, file_path TEXT,
		              directory_path TEXT, characters INT, lines INT,
		              file_summary TEXT)
	`)

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS classes
		             (id INTEGER PRIMARY KEY, name TEXT, file_path TEXT,
		              description TEXT)
	`)

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS functions  
		             (id INTEGER PRIMARY KEY, name TEXT, file_path TEXT,
		              description TEXT)
	`)

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS relationships
		             (id INTEGER PRIMARY KEY, entity1_id INT, entity2_id INT,
		              relationship TEXT)
	`)
	if err != nil {
		panic(err)
	}
	return db
}

func saveToFileDB(db *sql.DB, codeInfo CodeInfo) {

	// Insert file record
	result, err := db.Exec(`INSERT INTO files VALUES (NULL, ?, ?, ?, ?, ?, ?)`,
		codeInfo.Name, codeInfo.FilePath, codeInfo.DirectoryPath,
		codeInfo.Characters, codeInfo.Lines, codeInfo.FileSummary)

	if err != nil {
		return
	}

	// Get inserted id
	fileId, err := result.LastInsertId()

	// Add class records
	for _, class := range codeInfo.Classes {

		result, err := db.Exec(`INSERT INTO classes VALUES (NULL, ?, ?, ?)`,
			class.ClassName, codeInfo.FilePath, class.ClassDescription)

		if err != nil {
			continue
		}
		// Get class id
		classId, err := result.LastInsertId()

		// Add relationship
		_, err = db.Exec(`INSERT INTO relationships VALUES (NULL, ?, ?, ?)`,
			fileId, classId, "LOCATED_IN")

	}

	// Add class records
	for _, function := range codeInfo.Functions {

		result, err := db.Exec(`INSERT INTO functions VALUES (NULL, ?, ?, ?)`,
			function.FunctionName, codeInfo.FilePath, function.FunctionDescription)
		if err != nil {
			continue
		}
		// Get class id
		classId, err := result.LastInsertId()

		// Add relationship
		_, err = db.Exec(`INSERT INTO relationships VALUES (NULL, ?, ?, ?)`,
			fileId, classId, "LOCATED_IN")

	}
}

func processFile(file lib.File) (CodeInfo, error) {
	fmt.Println("scanning: " + file.FilePath)
	codeInfo := CodeInfo{
		Name:          filepath.Base(file.FilePath),
		FilePath:      file.FilePath,
		DirectoryPath: filepath.Dir(file.FilePath),
		Characters:    len(file.Content),
		Lines:         strings.Count(file.Content, "\n"),
		Classes:       make([]ClassInfo, 0),
		Functions:     make([]FunctionInfo, 0),
		FileSummary:   "",
	}

	treeData := lib.ParseFileTree(file)
	fmt.Printf("ParseFileTree(file): %v\n", treeData)
	llmInfo, err := lib.AddLLMInfo(treeData, file)
	if err != nil {
		return codeInfo, err
	}

	for _, classInfo := range llmInfo.Classes {
		codeInfo.Classes = append(codeInfo.Classes, ClassInfo{
			ClassName:        classInfo.ClassName,
			ClassDescription: classInfo.ClassDescription,
		})
	}

	for _, functionInfo := range llmInfo.Functions {
		codeInfo.Functions = append(codeInfo.Functions, FunctionInfo{
			FunctionName:        functionInfo.FunctionName,
			FunctionDescription: functionInfo.FunctionDescription,
		})
	}
	codeInfo.FileSummary = llmInfo.FileSummary

	return codeInfo, nil
}

func main() {
	start := time.Now()

	concurrency_limit := 4
	if len(os.Args) <= 1 {
		panic("No directory was appointed for scan")
	}

	fmt.Println("starting scan of: " + os.Args[1])
	db := setupDb()

	//   // Requests channel
	filesChannel := make(chan lib.File)

	//   // Responses channel
	codeInfosChannel := make(chan CodeInfo)

	var wg sync.WaitGroup

	for i := 0; i < concurrency_limit; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()

			for file := range filesChannel {
				startF := time.Now()

				codeInfo, err := processFile(file)
				if err != nil {
					fmt.Printf("File %v processing failed", file)
					continue
				}
				endF := time.Now()

				elapsedF := endF.Sub(startF)

				fmt.Printf("Total elapsed time %s for file %v", elapsedF.String(), file.FilePath)

				// Send response to channel
				codeInfosChannel <- codeInfo
			}
		}()
	}

	// Listen for responses
	go func() {
		for codeInfo := range codeInfosChannel {
			saveToFileDB(db, codeInfo)
		}
	}()

	for _, file := range lib.ScanFiles(os.Args[1]) {
		if filepath.Ext(file.FilePath) == ".js" {
			filesChannel <- file
		}
	}

	close(filesChannel)

	wg.Wait()

	close(codeInfosChannel)
	// fmt.Println(files[0].Content)
	end := time.Now()

	elapsed := end.Sub(start)

	fmt.Printf("Total elapsed time %s", elapsed.String())
}
