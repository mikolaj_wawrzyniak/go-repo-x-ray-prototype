module gitlab.com/mikolaj_wawrzyniak/go-repo-x-ray-prototype

go 1.20

require (
	github.com/mattn/go-sqlite3 v1.14.17
	github.com/smacker/go-tree-sitter v0.0.0-20230720070738-0d0a9f78d8f8
	github.com/tmc/langchaingo v0.0.0-20231107054554-0cff34d32fa3
)

require (
	github.com/dlclark/regexp2 v1.8.1 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/pkoukk/tiktoken-go v0.1.2 // indirect
)
