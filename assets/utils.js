// This function adds two numbers
function sum(a, b) {
    return a + b;
}

// this is a calculator class that perfomr basic math operations
class Calculator {
  constructor() {
    this.value = 0;
  }

  add(num) {
    this.value += num;
  }

  subtract(num) {
    this.value -= num;
  }

  multiply(num) {
    this.value *= num;
  }

  divide(num) {
    this.value /= num;
  }
}

